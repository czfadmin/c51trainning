//#include "INTRINS.H"
//#include "reg52.h"
#include "util/LCD.h"
#include "util/MUSIC.h"
#include "util/SOUNDPLAY.H"
#include "util/TEMP.h"
sbit LSA = P2 ^ 2;
sbit LSB = P2 ^ 3;
sbit LSC = P2 ^ 4;
uchar CNCHAR[] = "摄氏度";
uchar TABLE[7] = "Welcome";
uchar code SMGDUAN[17] = {0x66, 0x06, 0x5b, 0x06, 0x7f, 0x06, 0x3f, 0x5b,
	                  0x7f, 0x6f, 0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71};
uchar code TAB[8] = {0x7f, 0xbf, 0xdf, 0xef, 0xf7, 0xfb, 0xfd, 0xfe};
uchar code CHARCODE[1][8] = {
	0x00, 0x12, 0x14, 0x78, 0x14, 0x12, 0x00, 0x00,
};
volatile uchar keyValue;
volatile uchar stoping;
//#define PORT P0
void LcdDisplay(int);
void UsartConfiguration();

void DigDisplay() {
	uchar i;
	for (i = 0; i < 8; i++) {
		switch (i) {
		case (0):
			LSA = 0;
			LSB = 0;
			LSC = 0;
			break;
		case (1):
			LSA = 1;
			LSB = 0;
			LSC = 0;
			break;
		case (2):
			LSA = 0;
			LSB = 1;
			LSC = 0;
			break;
		case (3):
			LSA = 1;
			LSB = 1;
			LSC = 0;
			break;
		case (4):
			LSA = 0;
			LSB = 0;
			LSC = 1;
			break;
		case (5):
			LSA = 1;
			LSB = 0;
			LSC = 1;
			break;
		case (6):
			LSA = 0;
			LSB = 1;
			LSC = 1;
			break;
		case (7):
			LSA = 1;
			LSB = 1;
			LSC = 1;
			break;
		}
		P0 = SMGDUAN[i];
		Delay1ms(1);
		P0 = 0x00;
	}
}
void Int0Init() {
	//设置INT0
	IT0 = 1;  //跳变沿出发方式（下降沿）
	EX0 = 1;  //打开INT0的中断允许。
	EA = 1;   //打开总中断
}

void Int0Int() interrupt 0 {
	if (!stoping) {
		stoping = 1;
	} else {
		stoping = 0;
	}
}

void LcdDisplay(int temp)  // lcd显示
{
	uchar i, datas[] = {0, 0, 0, 0, 0};  //定义数组
	int j;
	float tp;
	if (temp < 0)  //当温度值为负数
	{
		LcdWriteCom(0x80 + 0x40);  //写地址 80表示初始地址
		SBUF = '-';  //将接收到的数据放入到发送寄存器
		while (!TI)
			;          //等待发送数据完成
		TI = 0;             //清除发送完成标志位
		LcdWriteData('-');  //显示负
		temp =
			temp -
			1; //因为读取的温度是实际温度的补码，所以减1，再取反求出原码
		temp = ~temp;
		tp = temp;
		temp = tp * 0.0625 * 100 + 0.5;
		//留两个小数点就*100，+0.5是四舍五入，因为C语言浮点数转换为整型的时候把小数点
		//后面的数自动去掉，不管是否大于0.5，而+0.5之后大于0.5的就是进1了，小于0.5的就
		//算由?0.5，还是在小数点后面。

	} else {
		LcdWriteCom(0x80 + 0x40);  //写地址 80表示初始地址
		LcdWriteData('+');       //显示正
		SBUF = '+';  //将接收到的数据放入到发送寄存器
		while (!TI)
			;  //等待发送数据完成
		TI = 0;    //清除发送完成标志位
		tp = temp;  //因为数据处理有小数点所以将温度赋给一个浮点型变量
		//如果温度是正的那么，那么正数的原码就是补码它本身
		temp = tp * 0.0625 * 100 + 0.5;
		//留两个小数点就*100，+0.5是四舍五入，因为C语言浮点数转换为整型的时候把小数点
		//后面的数自动去掉，不管是否大于0.5，而+0.5之后大于0.5的就是进1了，小于0.5的就
		//算加上0.5，还是在小数点后面。
	}
	datas[0] = temp / 10000;
	datas[1] = temp % 10000 / 1000;
	datas[2] = temp % 1000 / 100;
	datas[3] = temp % 100 / 10;
	datas[4] = temp % 10;

	LcdWriteCom(0x82 + 0x40);      //写地址 80表示初始地址
	LcdWriteData('0' + datas[0]);  //百位
	SBUF = '0' + datas[0];  //将接收到的数据放入到发送寄存器
	while (!TI)
		;  //等待发送数据完成
	TI = 0;

	LcdWriteCom(0x83 + 0x40);      //写地址 80表示初始地址
	LcdWriteData('0' + datas[1]);  //十位
	SBUF = '0' + datas[1];  //将接收到的数据放入到发送寄存器
	while (!TI)
		;  //等待发送数据完成
	TI = 0;

	LcdWriteCom(0x84 + 0x40);      //写地址 80表示初始地址
	LcdWriteData('0' + datas[2]);  //个位
	SBUF = '0' + datas[2];  //将接收到的数据放入到发送寄存器
	while (!TI)
		;  //等待发送数据完成
	TI = 0;

	LcdWriteCom(0x85 + 0x40);  //写地址 80表示初始地址
	LcdWriteData('.');       //显示 ‘.’
	SBUF = '.';  //将接收到的数据放入到发送寄存器
	while (!TI)
		;  //等待发送数据完成
	TI = 0;

	LcdWriteCom(0x86 + 0x40);      //写地址 80表示初始地址
	LcdWriteData('0' + datas[3]);  //显示小数点
	SBUF = '0' + datas[3];  //将接收到的数据放入到发送寄存器
	while (!TI)
		;  //等待发送数据完成
	TI = 0;

	LcdWriteCom(0x84);  // welcome
	for (j = 0; j < 7; j++) LcdWriteData(TABLE[j]);

	LcdWriteCom(0x87 + 0x40);      //写地址 80表示初始地址
	LcdWriteData('0' + datas[4]);  //显示小数点
	SBUF = '0' + datas[4];  //将接收到的数据放入到发送寄存器
	while (!TI)
		;  //等待发送数据完成
	TI = 0;

	LcdWriteCom(0x88 + 0x40);  //写地址 80表示初始地址
	LcdWriteData(0xdf);     //显示 ‘°’
	SBUF = 0xdf;  //将接收到的数据放入到发送寄存器
	while (!TI)
		;  //等待发送数据完成
	TI = 0;

	LcdWriteCom(0x89 + 0x40);  //写地址 80表示初始地址
	LcdWriteData('C');       //显示 ‘C’
	SBUF = 'C';  //将接收到的数据放入到发送寄存器
	while (!TI)
		;  //等待发送数据完成
	TI = 0;

	for (i = 0; i < 6; i++) {
		SBUF = CNCHAR[i];  //将接收到的数据放入到发送寄存器
		while (!TI)
			;  //等待发送数据完成
		TI = 0;
	}
}

void keyDown() {
	if (0 == key1) {
		Delay1ms(2);
		if (0 == key1) {
			keyValue = 0;
			stoping = 0;
		}
	}
	if (0 == key2) {
		Delay1ms(2);
		if (0 == key2) {
			keyValue = 1;
			stoping = 0;
		}
	}
	if (0 == key3) {
		Delay1ms(3);
		if (0 == key3) {
			keyValue = 2;
			stoping = 0;
		}
	}
	if (0 == key4) {
		Delay1ms(1);
		if (0 == key4) {
			keyValue = 3;
			stoping = 0;
		}
	}
}
void main() {
	//uchar t, j, i;
	uchar ledNum;
	ledNum = "";
	stoping = 1;
	InitialSound();
	Int0Init();
	LcdInit();                 //初始化LCD1602
	LcdWriteCom(0x88 + 0x40);  //写地址 80表示初始地址
	LcdWriteData('C');
	while (1) {
		keyDown();
		if (!stoping) {

			switch (keyValue) {
			case 0: {
				Play(MUSICGIRL, 0, 3, 360);
				Delay1ms(1);
			} break;
			case 1: {
				Play(MUSICSAME, 0, 3, 360);
				Delay1ms(1);
			} break;
			case 2: {
				Play(MUSICTWO, 0, 3, 360);
				Delay1ms(1);
			} break;
			case 3: {
				while (1) {
					DigDisplay();
					if (0 == key1 || 0 == key2 ||
					    0 == key3) {
						Delay1ms(1);
						if (0 == key1 ||
						    0 == key2 ||
						    0 == key3)
							break;
					}
				}

			} break;
			}
		}

		LcdDisplay(Ds18b20ReadTemp());
		
	}
}